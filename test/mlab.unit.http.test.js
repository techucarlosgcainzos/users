/* eslint-disable no-console */
const test = require('ava');
const nock = require('nock');

const mlab = require('../lib/mlab');
const responseUsers = require('./fixtures/users');
const responseUser = require('./fixtures/user');

test('indexUsers: return all users', async (t) => {
  nock('https://api.mlab.com')
    .get('/api/1/databases/techu/collections/user?apiKey=123')
    .reply(200, responseUsers);

  const req = {
    method: 'GET',
    body: {},
    params: { 0: '' },
  };

  const users = await mlab.indexUsers(req);
  t.deepEqual(users, responseUsers);
});

test('indexUsers: return user by id', async (t) => {
  nock('https://api.mlab.com')
    .get('/api/1/databases/techu/collections/user?q=%7B%22id%22:%223%22%7D&apiKey=123')
    .reply(200, responseUser);

  const req = {
    method: 'GET',
    body: {},
    params: { 0: '/3' },
  };

  const user = await mlab.indexUsers(req);
  t.deepEqual(user, responseUser);
});

test('indexUsers: return error', async (t) => {
  nock('https://api.mlab.com')
    .get('/api/1/databases/techu/collections/user?q=%7B%22id%22:%223%22%7D&apiKey=123')
    .reply(500);

  const req = {
    method: 'GET',
    body: {},
    params: { 0: '/3' },
  };

  try {
    await mlab.indexUsers(req);
  } catch (e) {
    t.deepEqual(e.message, 'Request failed with status code 500');
  }
});

test('modifyUsers: call with empty array if no body', async (t) => {
  nock('https://api.mlab.com')
    .put('/api/1/databases/techu/collections/user?q=%7B%22id%22:%223%22%7D&apiKey=123', [])
    .reply(200, { n: 0, removed: 1 });

  const req = {
    method: 'PUT',
    params: { 0: '/3' },
  };

  const del = await mlab.modifyUsers(req);
  t.deepEqual(del, { n: 0, removed: 1 });
});

test('modifyUsers: call with set object if body', async (t) => {
  nock('https://api.mlab.com')
    .put('/api/1/databases/techu/collections/user?q=%7B%22id%22:%223%22%7D&apiKey=123',
      { $set: { first_name: 'Tony' } })
    .reply(200, { n: 1 });

  const req = {
    method: 'PUT',
    body: { first_name: 'Tony' },
    params: { 0: '/3' },
  };

  const mod = await mlab.modifyUsers(req);
  t.deepEqual(mod, { n: 1 });
});

test('modifyUsers: return error', async (t) => {
  nock('https://api.mlab.com')
    .put('/api/1/databases/techu/collections/user?q=%7B%22id%22:%223%22%7D&apiKey=123')
    .reply(500);

  const req = {
    method: 'PUT',
    body: {},
    params: { 0: '/3' },
  };

  try {
    await mlab.modifyUsers(req);
  } catch (e) {
    t.deepEqual(e.message, 'Request failed with status code 500');
  }
});
