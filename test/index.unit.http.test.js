const test = require('ava');
const sandbox = require('sinon').createSandbox();
const createJWKSMock = require('mock-jwks').default;

const gcpFunction = require('../index');
const userController = require('../controllers/userController');
const accountController = require('../controllers/accountController');

let jwksMock;

test.beforeEach(async () => {
  jwksMock = createJWKSMock('https://techucarlosgcainzos.eu.auth0.com');
});

test.afterEach(async () => {
  await jwksMock.stop();
});

test('users: GET should call handleUsers', async (t) => {
  const handleUsersSpy = sandbox.spy(userController, 'handleUsers');
  await jwksMock.start();
  const accessToken = jwksMock.token({
    aud: 'https://europe-west1-micro-vine-218919.cloudfunctions.net/users/',
    iss: 'https://techucarlosgcainzos.eu.auth0.com/',
  });
  const req = {
    method: 'GET',
    params: { 0: '' },
    body: {},
    headers: {
      authorization: `Bearer ${accessToken}`,
    },
  };
  const res = {
    send: () => {},
    status: () => res,
    set: () => {},
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  sandbox.spy(res, 'set');

  await gcpFunction.users(req, res);
  t.true(handleUsersSpy.called);
});

test('users: OPTIONS should response with status 200', async (t) => {
  await jwksMock.start();
  const accessToken = jwksMock.token({
    aud: 'https://europe-west1-micro-vine-218919.cloudfunctions.net/users/',
    iss: 'https://techucarlosgcainzos.eu.auth0.com/',
  });
  const req = {
    method: 'OPTIONS',
    headers: {
      authorization: `Bearer ${accessToken}`,
    },
    params: { 0: '' },
  };
  const res = {
    send: () => {},
    status: () => res,
    set: () => {},
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  sandbox.spy(res, 'set');

  await gcpFunction.users(req, res);
  t.deepEqual(res.status.firstCall.args, [200]);
  // t.deepEqual(res.send.firstCall.args, [{ message: 'Method not allowed' }]);
});

test('users: POST with user data should call createUser', async (t) => {
  const createUserSpy = sandbox.spy(userController, 'createUser');
  await jwksMock.start();
  const accessToken = jwksMock.token({
    aud: 'https://europe-west1-micro-vine-218919.cloudfunctions.net/users/',
    iss: 'https://techucarlosgcainzos.eu.auth0.com/',
  });
  const req = {
    method: 'POST',
    headers: {
      authorization: `Bearer ${accessToken}`,
    },
    params: { 0: '' },
    body: {
      first_name: 'truco',
      last_name: 'dent',
      email: 'truco@dent.com',
      password: '1234',
    },
  };
  const res = {
    send: () => {},
    status: () => res,
    set: () => {},
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  sandbox.spy(res, 'set');

  await gcpFunction.users(req, res);
  t.true(createUserSpy.calledOnce);
});

test('users: PUT with user data should call updateUser', async (t) => {
  const updateUserSpy = sandbox.spy(userController, 'updateUser');
  await jwksMock.start();
  const accessToken = jwksMock.token({
    aud: 'https://europe-west1-micro-vine-218919.cloudfunctions.net/users/',
    iss: 'https://techucarlosgcainzos.eu.auth0.com/',
  });
  const req = {
    method: 'PUT',
    headers: {
      authorization: `Bearer ${accessToken}`,
    },
    params: { 0: '' },
    body: {
      first_name: 'truco',
      last_name: 'dent',
      email: 'truco@dent.com',
      password: '1234',
    },
  };
  const res = {
    send: () => {},
    status: () => res,
    set: () => {},
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  sandbox.spy(res, 'set');

  await gcpFunction.users(req, res);
  t.true(updateUserSpy.calledOnce);
});

test('users: DELETE with user data should call deleteUser', async (t) => {
  const deleteUserSpy = sandbox.spy(userController, 'deleteUser');
  await jwksMock.start();
  const accessToken = jwksMock.token({
    aud: 'https://europe-west1-micro-vine-218919.cloudfunctions.net/users/',
    iss: 'https://techucarlosgcainzos.eu.auth0.com/',
  });
  const req = {
    method: 'DELETE',
    headers: {
      authorization: `Bearer ${accessToken}`,
    },
    params: { 0: '' },
    body: {
      id: 2,
    },
  };
  const res = {
    send: () => {},
    status: () => res,
    set: () => {},
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  sandbox.spy(res, 'set');

  await gcpFunction.users(req, res);
  t.true(deleteUserSpy.calledOnce);
});

test('users: GET should have CORS enabled', async (t) => {
  await jwksMock.start();
  const accessToken = jwksMock.token({
    aud: 'https://europe-west1-micro-vine-218919.cloudfunctions.net/users/',
    iss: 'https://techucarlosgcainzos.eu.auth0.com/',
  });
  const req = {
    method: 'GET',
    headers: {
      authorization: `Bearer ${accessToken}`,
    },
    params: { 0: '' },
  };
  const res = {
    send: () => {},
    status: () => res,
    set: () => {},
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  sandbox.spy(res, 'set');

  await gcpFunction.users(req, res);
  t.deepEqual(res.set.callCount, 5);
});

test('accounts: POST should call createAccount', async (t) => {
  const createAccountStub = sandbox.stub(accountController, 'createAccount')
    .returns(new Promise((resolve) => {
      resolve();
    }));
  await jwksMock.start();
  const accessToken = jwksMock.token({
    aud: 'https://europe-west1-micro-vine-218919.cloudfunctions.net/users/',
    iss: 'https://techucarlosgcainzos.eu.auth0.com/',
  });
  const req = {
    method: 'POST',
    headers: {
      authorization: `Bearer ${accessToken}`,
    },
    params: { 0: '' },
    body: {},
  };
  const res = {
    send: () => {},
    status: () => res,
    set: () => {},
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  sandbox.spy(res, 'set');

  await gcpFunction.accounts(req, res);
  t.true(createAccountStub.calledOnce);
});

test('accounts: PUT should call createAction', async (t) => {
  const createActionStub = sandbox.stub(accountController, 'createAction')
    .returns(new Promise((resolve) => {
      resolve();
    }));
  await jwksMock.start();
  const accessToken = jwksMock.token({
    aud: 'https://europe-west1-micro-vine-218919.cloudfunctions.net/users/',
    iss: 'https://techucarlosgcainzos.eu.auth0.com/',
  });
  const req = {
    method: 'PUT',
    headers: {
      authorization: `Bearer ${accessToken}`,
    },
    params: { 0: '' },
    body: {
      ammount: 10,
      action: 'Deposit',
    },
  };
  const res = {
    send: () => {},
    status: () => res,
    set: () => {},
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  sandbox.spy(res, 'set');

  await gcpFunction.accounts(req, res);
  t.true(createActionStub.calledOnce);
});
