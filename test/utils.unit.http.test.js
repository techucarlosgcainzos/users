const test = require('ava');
const utils = require('../lib/utils');

test('sumMoney: 100.0 + 100.0 = 200.000', async (t) => {
  const sum = await utils.sumMoney('100.0', '100.0');
  t.is(sum, '200.000');
});

test('sumMoney: 100.6 + 100.6 = 201.200', async (t) => {
  const sum = await utils.sumMoney('100.6', '100.6');
  t.is(sum, '201.200');
});

test('sumMoney: 100.5 + 100.5 = 201.000', async (t) => {
  const sum = await utils.sumMoney('100.5', '100.5');
  t.is(sum, '201.000');
});

test('sumMoney: 100 + 100 = 200.000', async (t) => {
  const sum = await utils.sumMoney('100', '100');
  t.is(sum, '200.000');
});

test('sumMoney: 100.2 + 100.2 = 200.400', async (t) => {
  const sum = await utils.sumMoney('100.20', '100.2');
  t.is(sum, '200.400');
});

test('sumMoney: -100 + 100.76 = 0.760', async (t) => {
  const sum = await utils.sumMoney('-100.00', '100.76');
  t.is(sum, '0.760');
});

test('sumMoney: 100 - 100.76 = -0.760', async (t) => {
  const sum = await utils.sumMoney('100.00', '-100.76');
  t.is(sum, '-0.760');
});

test('sumMoney: 100 - 300.76 = -0.760', async (t) => {
  const sum = await utils.sumMoney('100.00', '-300.76');
  t.is(sum, '-200.760');
});
