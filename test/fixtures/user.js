module.exports = [
  {
    _id: {
      $oid: '5bd09c63636d69ee5ca0045b',
    },
    id: 3,
    first_name: 'Dennet',
    last_name: 'Mackelworth',
    email: 'dmackelworth2@army.mil',
    gender: 'Male',
    accounts: [
      {
        iban: 'HR95 2254 1969 9076 9344 8',
        balance: '-226262.82',
        movements: [],
      },
      {
        iban: 'FR727824881016RYPO3ZLV21E46',
        balance: '-100',
        movements: [
          { amount: '100', date_created: '2018-12-16T22:36:08.837Z' },
          { amount: '-100', date_created: '2018-12-16T22:39:08.837Z' },
        ],
      },
      {
        iban: 'FR11 8431 5187 18LW JGQV XOYW O22',
        balance: '-597153.33',
        movements: [],
      },
      {
        iban: 'KZ62 714C QZRP KH3U HDR1',
        balance: '-567022.83',
        movements: [],
      },
      {
        iban: 'SI66 5978 1197 5371 778',
        balance: '630962.02',
        movements: [],
      },
    ],
  },
];
