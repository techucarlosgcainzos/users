// [START functions_http_integration_test]
const test = require('ava');
const Supertest = require('supertest');
const users = require('./fixtures/notInsertedUsers');

const supertest = Supertest('http://localhost:8010/techU/us-central1');
const mlab = Supertest('https://api.mlab.com/api/1/databases/techu/collections');
const auth0 = Supertest('https://techucarlosgcainzos.eu.auth0.com');
let token;


test.before(() => {
  console.log('Testing Users API');
  const options = {
    body: {
      client_id: 'MJX4u2hKtn4Wudh6Z1CgLWyP3VDxdmFl',
      client_secret: process.env.AUTH_API_KEY,
      audience: 'https://europe-west1-micro-vine-218919.cloudfunctions.net/users/',
      grant_type: 'client_credentials',
    },
  };
  auth0
    .post('/oauth/token')
    .set('content-type', 'application/json')
    .send(options.body)
    .then((response) => {
      token = response.body;
    });
});

test.afterEach.always.cb('cleanup', (t) => {
  mlab
    .put('/user')
    .query({ apiKey: process.env.MLAB_API_KEY })
    .send([])
    .expect(200)
    .then(() => {
      t.end();
    });
});

test.serial.cb('users-get-index: should return list of Users', (t) => {
  mlab
    .post('/user')
    .query({ apiKey: process.env.MLAB_API_KEY })
    .send(users)
    .expect(200)
    .then((resMlab) => {
      t.is((resMlab.body.n), 2);
      supertest
        .get('/users')
        .set('authorization', `Bearer ${token}`)
        .expect(200)
        .then((resUsr) => {
          t.is((resUsr.body.length), 2);
        })
        .catch((err) => {
          console.log(err);
        });
      t.end();
    })
    .catch((error) => {
      console.log(error);
    });
});

test.skip('users-get-id: should return Users details', () => {
  /**
   * Functions simulator does not parse URL params.
   * This test should be made as system test
   */
  // supertest
  //   .get('/users/3')
  //   .expect(200)
  //   .expect((response) => {
  //     t.deepEqual(response.body, user);
  //   })
  //   .end(t.end);
});

test.serial.cb('users-post-user: should return 204', (t) => {
  supertest
    .post('/users')
    .set('authorization', `Bearer ${token}`)
    .send({
      id: 2000,
      first_name: 'trato',
      last_name: 'magic',
      email: 'trato@magic.com',
      password: '1234',
    })
    .expect(201)
    .then(() => {
      mlab
        .put('/user')
        .query({ q: { id: 2000 }, apiKey: process.env.MLAB_API_KEY })
        .send([])
        .expect(200)
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));
  t.end();
});

test.skip('users-delete-id: should return #Users deleted', () => {
  /**
   * Functions simulator does not parse URL params.
   * This test should be made as system test
   */
  // supertest
  //   .post('/users')
  //   .send({
  //     id: 101,
  //     first_name: 'bruce',
  //     last_name: 'wayne',
  //     email: 'bruce@wayne.com',
  //     password: '1234',
  //   })
  //   .expect(201);
  //
  // supertest
  //   .delete('/users/101')
  //   .expect(200)
  //   .end(t.end);
});

test.skip('users-put-id: should return #Users deleted', () => {
  /**
   * Functions simulator does not parse URL params.
   * This test should be made as system test
   */
  // supertest
  //   .post('/users')
  //   .send({
  //     id: 102,
  //     first_name: 'eric',
  //     last_name: 'wizard',
  //     email: 'eric@magician.com',
  //     password: '1234',
  //   })
  //   .expect(201);
  //
  // supertest
  //   .put('/users/102')
  //   .send({
  //     email: 'eric@magic.com',
  //   })
  //   .expect(200)
  //   .end(t.end);
});

test.serial.cb('users-delete-email: should return 204', (t) => {
  const user = {
    id: 1000,
    first_name: 'truco',
    last_name: 'dent',
    email: 'truco@dent.com',
    password: '1234',
  };

  mlab
    .post('/user')
    .query({ apiKey: process.env.MLAB_API_KEY })
    .send(user)
    .expect(200)
    .then((resMlab) => {
      t.is((resMlab.body.id), 1000);
      supertest
        .delete('/users')
        .set('authorization', `Bearer ${token}`)
        .send({ email: 'truco@dent.com' })
        .expect(204)
        .catch((err) => { console.log(err); });
    })
    .catch((error) => {
      console.log(error);
    });
  t.end();
});
// [END functions_http_integration_test]
