const test = require('ava');
const sandbox = require('sinon').createSandbox();
const createJWKSMock = require('mock-jwks').default;

const gcpFunction = require('../index');
const auth = require('../lib/auth');

let jwksMock;

// test.beforeEach(async () => {
//   jwksMock = createJWKSMock('https://techucarlosgcainzos.eu.auth0.com');
// });

// test.afterEach(async () => {
//   await jwksMock.stop();
// });


test('auth: without token response status 401', async (t) => {
  const req = {
    method: 'GET',
  };
  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  await gcpFunction.users(req, res);
  t.deepEqual(res.status.firstCall.args, [401]);
  t.deepEqual(res.send.firstCall.args, ['No authorization token found.']);
});

test('auth: with bad format response status 401', async (t) => {
  const req = {
    method: 'GET',
    headers: {
      authorization: 'Bearer bad token',
    },
  };
  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  await gcpFunction.users(req, res);
  t.deepEqual(res.status.firstCall.args, [401]);
  t.deepEqual(res.send.firstCall.args, ['Bad credential format.']);
});

test('auth: without Bearer response status 401', async (t) => {
  const req = {
    method: 'GET',
    headers: {
      authorization: 'Rarer badToken',
    },
  };
  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  await gcpFunction.users(req, res);
  t.deepEqual(res.status.firstCall.args, [401]);
  t.deepEqual(res.send.firstCall.args, ['Bad credential format.']);
});

test('auth: with no JWKS response status 401', async (t) => {
  jwksMock = createJWKSMock('https://techucarlosgcainzos.eu.auth0.com');
  // await jwksMock.start();
  const accessToken = jwksMock.token({
    aud: 'https://europe-west1-micro-vine-218919.cloudfunctions.net/users/',
    iss: 'https://techucarlosgcainzos.eu.auth0.com/',
  });
  const req = {
    method: 'GET',
    params: { 0: '' },
    headers: {
      authorization: `Bearer ${accessToken}`,
    },
  };
  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  await t.throwsAsync(gcpFunction.users(req, res), 'Invalid token.');
  t.deepEqual(res.status.firstCall.args, [401]);
  t.deepEqual(res.send.firstCall.args, ['Invalid token']);
  sandbox.restore();
});

test('auth: OPTIONS status 200', async (t) => {
  const req = {
    method: 'OPTIONS',
  };
  const res = {
    send: () => {},
    set: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  sandbox.spy(res, 'set');

  await gcpFunction.users(req, res);
  t.deepEqual(res.status.firstCall.args, [200]);
  // t.deepEqual(res.send.firstCall.args, ['No authorization token found.']);
});
