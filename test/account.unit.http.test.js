const test = require('ava');
const sandbox = require('sinon').createSandbox();

const accountController = require('../controllers/accountController');
const mlab = require('../lib/mlab');
const responseUser = require('./fixtures/user');

const user = {
  nickname: 'tony',
  name: 'dmackelworth2@army.mil',
  picture: 'https://s.gravatar.com/avatar/fa9472d548112c47cf9eb59e0ad04865?s=480&r=pg&d=https%3A%2F%2Fcdn.auth0.com%2Favatars%2Ftr.png',
  updated_at: '2018-12-10T16:11:07.016Z',
  email: 'dmackelworth2@army.mil',
  email_verified: false,
  iss: 'https://techucarlosgcainzos.eu.auth0.com/',
  sub: 'auth0|5c0e70e637922454b90db070',
  aud: 'DZPgR27e4YGbVttjy94cVJByBYYdq0y9',
  iat: 1544961312,
  exp: 1544997312,
};

const req = {
  method: 'POST',
  params: {
    0: '',
    user,
  },
};

let indexUsersStub = {};
let modifyUsersStub = {};
let createUserStub = {};

test.before(() => {
  indexUsersStub = sandbox.stub(mlab, 'indexUsers');
  modifyUsersStub = sandbox.stub(mlab, 'modifyUsers').returns({ n: 1 });

  const userNoAccounts = JSON.parse(JSON.stringify(responseUser));
  userNoAccounts[0].accounts = [];
  createUserStub = sandbox.stub(mlab, 'createUser').returns(userNoAccounts[0]);
});

test.afterEach(() => {
  console.log('Restoring...');
  indexUsersStub.restore();
  modifyUsersStub.restore();
  createUserStub.restore();
  sandbox.restore();
});

test('createAccount: should response 201', async (t) => {
  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  indexUsersStub.returns(responseUser);

  await accountController.createAccount(req, res)
    .then(() => {
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [201]);
      t.true('iban' in res.send.firstCall.args[0]);
    });
});

test('createAccount: should create user if not exists', async (t) => {
  const res = {
    send: () => {},
    status: () => res,
  };

  req.params.user = {};

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  indexUsersStub.returns([]);

  await accountController.createAccount(req, res)
    .then(() => {
      t.true(createUserStub.calledOnce);
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [201]);
      t.true('iban' in res.send.firstCall.args[0]);
      req.params.user = { user };
    });
});

test('createAction: should response 204', async (t) => {
  const res = {
    send: () => {},
    status: () => res,
  };

  req.body = {
    action: 'Deposit',
    ammount: '100.76',
  };
  req.params[0] = '/FR727824881016RYPO3ZLV21E46';

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  indexUsersStub.returns(responseUser);

  await accountController.createAction(req, res)
    .then(() => {
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [204]);
      delete req.body;
      req.params[0] = '';
    });
});

test('createAction: without IBAN should response 400', async (t) => {
  const res = {
    send: () => {},
    status: () => res,
  };

  req.body = {
    action: 'Deposit',
    ammount: '100.50',
  };
  req.params[0] = '';

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  indexUsersStub.returns(responseUser);

  await accountController.createAction(req, res)
    .then(() => {
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [400]);
      delete req.body;
    });
});

test('createAction: without valid action should response 400', async (t) => {
  const res = {
    send: () => {},
    status: () => res,
  };

  req.body = {
    action: 'BadAction',
    ammount: '100',
  };
  req.params[0] = '/FR727824881016RYPO3ZLV21E46';

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  indexUsersStub.returns(responseUser);

  await accountController.createAction(req, res)
    .then(() => {
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [400]);
      delete req.body;
      req.params[0] = '';
    });
});

test('createAction: if iban not in user should response 404', async (t) => {
  const res = {
    send: () => {},
    status: () => res,
  };

  req.body = {
    action: 'Deposit',
    ammount: '100',
  };
  req.params[0] = '/FR727824881016RYPO3ZLV21E51';

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');
  indexUsersStub.returns(responseUser);

  await accountController.createAction(req, res)
    .then(() => {
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [404]);
      delete req.body;
      req.params[0] = '';
    });
});
