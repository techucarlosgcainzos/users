const test = require('ava');
const sandbox = require('sinon').createSandbox();
const nock = require('nock');

const userController = require('../controllers/userController');
const responseUsers = require('./fixtures/users');
const responseUser = require('./fixtures/user');

test('handleUsers: should response with list of users', (t) => {
  nock('https://api.mlab.com')
    .get('/api/1/databases/techu/collections/user?apiKey=123')
    .reply(200, responseUsers);

  const req = {
    method: 'GET',
    params: { 0: '' },
  };

  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  return userController.handleUsers(req, res)
    .then(() => {
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [200]);
      t.deepEqual(res.send.firstCall.args, [responseUsers]);
    });
});

test('handleUsers: should response with status 500 if error', (t) => {
  nock('https://api.mlab.com')
    .get('/api/1/databases/techu/collections/user?apiKey=123')
    .reply(500);

  const req = {
    method: 'GET',
    params: { 0: '' },
  };

  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');


  return userController.handleUsers(req, res)
    .then(() => {
      t.deepEqual(res.status.firstCall.args, [500]);
      t.deepEqual(res.send.firstCall.args, [{ message: 'Request failed with status code 500' }]);
      t.true(res.send.calledOnce);
    });
});

test('handleUsers: by ID should response with user', (t) => {
  nock('https://api.mlab.com')
    .get('/api/1/databases/techu/collections/user?q=%7B%22id%22:%223%22%7D&apiKey=123')
    .reply(200, responseUser);

  const req = {
    method: 'GET',
    body: {},
    params: { 0: '/3' },
  };

  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  return userController.handleUsers(req, res)
    .then(() => {
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [200]);
      t.deepEqual(res.send.firstCall.args, [responseUser]);
    });
});

test('handleUsers: by email should response with user', (t) => {
  nock('https://api.mlab.com')
    .get('/api/1/databases/techu/collections/user?q=%7B%22email%22:%22dmackelworth2@army.mil%22%7D&apiKey=123')
    .reply(200, responseUser);

  const req = {
    method: 'GET',
    params: { 0: '' },
    body: {
      email: 'dmackelworth2@army.mil',
    },
  };

  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  return userController.handleUsers(req, res)
    .then(() => {
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [200]);
      t.deepEqual(res.send.firstCall.args, [responseUser]);
    });
});

test('handleUsers: not existing ID should response with 404', (t) => {
  nock('https://api.mlab.com')
    .get('/api/1/databases/techu/collections/user?q=%7B%22id%22:%223%22%7D&apiKey=123')
    .reply(404, []);

  const req = {
    method: 'GET',
    body: {},
    params: { 0: '/3' },
  };

  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  return userController.handleUsers(req, res)
    .then(() => {
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [404]);
      t.deepEqual(res.send.firstCall.args, [{ message: 'User not found' }]);
    });
});

test('createUser: should response with 201', (t) => {
  nock('https://api.mlab.com')
    .post('/api/1/databases/techu/collections/user?apiKey=123')
    .reply(201, responseUser);

  const req = {
    method: 'POST',
    body: {
      first_name: 'truco',
      last_name: 'dent',
      email: 'truco@dent.com',
    },
  };

  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  return userController.createUser(req, res)
    .then(() => {
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [201]);
    });
});

test('createUser: should response with status 500 if error', (t) => {
  nock('https://api.mlab.com')
    .post('/api/1/databases/techu/collections/user?apiKey=123')
    .reply(500);

  const req = {
    method: 'POST',
    body: {
      first_name: 'truco',
      last_name: 'dent',
      email: 'truco@dent.com',
      password: 1234,
    },
  };

  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');


  return userController.createUser(req, res)
    .then(() => {
      t.deepEqual(res.status.firstCall.args, [500]);
      t.deepEqual(res.send.firstCall.args, [{ message: 'Unable to create user' }]);
      t.true(res.send.calledOnce);
    });
});

test('createUser: should response with status 500 if MLab timeout', (t) => {
  nock('https://api.mlab.com')
    .post('/api/1/databases/techu/collections/user?apiKey=123')
    .delayConnection(3100)
    .reply(200);

  const req = {
    method: 'POST',
    body: {
      first_name: 'truco',
      last_name: 'dent',
      email: 'truco@dent.com',
      password: '1234',
      id: 100,
    },
  };

  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');


  return userController.createUser(req, res)
    .then(() => {
      t.deepEqual(res.status.firstCall.args, [500]);
      t.deepEqual(res.send.firstCall.args, [{ message: 'Unable to create user' }]);
      t.true(res.send.calledOnce);
    });
});

test('updateUser: by ID should response with 204', (t) => {
  nock('https://api.mlab.com')
    .get('/api/1/databases/techu/collections/user?q=%7B%22id%22:%223%22%7D&apiKey=123')
    .reply(200, responseUser)
    .put('/api/1/databases/techu/collections/user?q=%7B%22id%22:%223%22%7D&apiKey=123',
      {
        $set: { first_name: 'Tony' },
      })
    .reply(200, { n: 1 });

  const req = {
    method: 'PUT',
    body: {
      first_name: 'Tony',
    },
    params: { 0: '/3' },
  };

  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  return userController.updateUser(req, res)
    .then(() => {
      t.deepEqual(res.status.firstCall.args, [204]);
      t.true(res.status.calledOnce);
    });
});

test('updateUser: by not existing ID should response with 404', (t) => {
  nock('https://api.mlab.com')
    .get('/api/1/databases/techu/collections/user?q=%7B%22id%22:%223%22%7D&apiKey=123')
    .reply(200, [])
    .put('/api/1/databases/techu/collections/user?q=%7B%22id%22:%223%22%7D&apiKey=123')
    .reply(204);

  const req = {
    method: 'PUT',
    body: {
      first_name: 'Tony',
    },
    params: { 0: '/3' },
  };

  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  return userController.updateUser(req, res)
    .then(() => {
      t.deepEqual(res.status.firstCall.args, [404]);
      t.true(res.status.calledOnce);
    });
});

test('deleteUser: by ID should response with 204', (t) => {
  nock('https://api.mlab.com')
    .put('/api/1/databases/techu/collections/user?q=%7B%22id%22:%22100%22%7D&apiKey=123', [])
    .reply(200, { n: 0, removed: 1 });

  const req = {
    method: 'DELETE',
    body: {},
    params: { 0: '/100' },
  };

  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  return userController.deleteUser(req, res)
    .then(() => {
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [204]);
    });
});

test('deleteUser: by email should response with 204', (t) => {
  nock('https://api.mlab.com')
    .put('/api/1/databases/techu/collections/user?q=%7B%22email%22:%22dmackelworth2@army.mil%22%7D&apiKey=123', [])
    .reply(200, { n: 0, removed: 1 });

  const req = {
    method: 'GET',
    params: { 0: '' },
    body: {
      email: 'dmackelworth2@army.mil',
    },
  };

  const res = {
    send: () => {},
    status: () => res,
  };

  sandbox.spy(res, 'status');
  sandbox.spy(res, 'send');

  return userController.deleteUser(req, res)
    .then(() => {
      t.true(res.send.calledOnce);
      t.deepEqual(res.status.firstCall.args, [204]);
    });
});
