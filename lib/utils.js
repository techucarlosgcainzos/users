/* eslint-disable no-console */
const bcrypt = require('bcrypt');

module.exports = {
  toType(obj) {
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
  },
  /**
   * Hash data with bcrypto library.
   * @param {Object} data - Data to be hashed
   * @returns {String} Hashed data string with a 10 round salt
   */
  hash(data) {
    console.log('> utils.hash: ');
    return bcrypt.hashSync(data, 10);
  },
  /**
   * Check hashed passwords with bcrypto library.
   * @param {Object} sentPassword - Password which user sends.
   * @param {Object} userHashPassword - Password which is stored into DB.
   * @returns {Bool} Comparasion between passwords.
   */
  checkPassword(sentPassword, userHashPassword) {
    console.log('> utils.checkPassword');
    return bcrypt.compareSync(sentPassword, userHashPassword);
  },
  /**
   * Sum Money.
   * @param {String} sum1 - String Money.
   * @param {String} sum2 - String Money.
   * @returns {String} Sum of sum1 plus sum2.
   */
  sumMoney(sum1, sum2) {
    console.log(`> utils.sumMoney: ${sum1} + ${sum2}`);
    const splitSum1 = sum1.split('.', 2);
    const splitSum2 = sum2.split('.', 2);
    let resDecimal;
    let sign = '';

    if (!splitSum1[1]) {
      splitSum1.push('000');
    }

    if (!splitSum2[1]) {
      splitSum2.push('000');
    }

    const sumDecimal = Number(splitSum1[1].substring(0, 3).padEnd(3, '0'))
      + Number(splitSum2[1].substring(0, 3).padEnd(3, '0'));
    let sumInteger = Number(splitSum1[0]) + Number(splitSum2[0]);

    if (sumDecimal >= 1000) {
      resDecimal = sumDecimal % 1000;
      sumInteger += 1;
    } else {
      resDecimal = sumDecimal;
    }

    if ((Number(splitSum2[0]) < 0) && (sumInteger === 0)) {
      sign = '-';
    }
    return `${sign}${sumInteger}.${resDecimal.toString().padEnd(3, '0')}`;
  },
  /**
   * Get Parms from request.
   * @param {Object} req - Express request.
   * @returns {Bool} True if params found.
   */
  params(req) {
    console.log('> utils.params');
    if (!req.params) {
      return false;
    }
    const path = req.params['0'];
    if (!path) {
      return false;
    }
    req.params.parts = path.split('/');
    while (req.params.parts[0] === '') {
      req.params.parts.shift();
    }
    if (req.params.parts.length === 0) {
      delete req.params.parts;
      console.log('no params');
      return false;
    }
    console.log('params');
    return true;
  },
  /**
   * Enable CORS headers to allow authentication request.
   * @param {Object} res - Express response.
   * @returns {Bool} True if params found.
   */
  async enableCORSHeaders(res) {
    console.log('> utils.enableCORSHeaders');
    res.set('Access-Control-Max-Age', '3600');
    res.set('Access-Control-Allow-Origin', 'https://techucarlosgcainzos.bitbucket.io');
    res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, OPTIONS');
    res.set('Access-Control-Allow-Headers', 'Content-Type, authorization');
    res.set('Access-Control-Allow-Credentials', 'true');
    return true;
  },
};
