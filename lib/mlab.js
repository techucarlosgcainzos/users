/* eslint-disable no-console */

const axios = require('axios');
const utils = require('../lib/utils');

const mlabCollection = process.env.MLAB_COLLECTION || 'user';

/**
 * Handle Axios Error.
 * @param {Object} error - Axios error.
 * @param {String} action - Action to log.
 * @returns {null} .
 */
function handleError(error, action) {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log(`< mlab.${action}: Response error: `,
      'status code: ', error.response.status,
      ', data: ', error.response.data,
      ', headers: ', error.response.headers);
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    console.log(`< mlab.${action}:: No response received: `, error.stack);
  } else {
    // Something happened in setting up the request that triggered an Error
    console.log(`< mlab.${action}:: Error: `, error.message);
  }
  console.log(`< mlab.${action}:: Stack: `, error.stack);
}

/**
 * Get User(s) from MLab".
 *
 * @param {Object} req Cloud Function request context.
 * @return {Promise} A list of users or error.
 */
async function indexUsers(req) {
  console.log('> mlab.indexUsers');

  const request = axios.get(`https://api.mlab.com/api/1/databases/techu/collections/${mlabCollection}`,
    this.requestOptions(req));

  return request
    .then(result => result.data)
    .catch((error) => {
      handleError(error, 'indexUsers');
      return Promise.reject(error);
    });
}

/**
 * Create MLab User".
 *
 * @param {Object} req Cloud Function request context.
 * @param {String} deleteUser delete will be performed.
 * @return {Promise} A list of users or error.
 */
async function createUser(req) {
  const dbKey = process.env.MLAB_API_KEY || '123';
  console.log('> mlab.createUser');

  const request = axios
    .post(`https://api.mlab.com/api/1/databases/techu/collections/${mlabCollection}`,
      {
        id: Math.floor(Math.random() * 9999999999) + 1,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        accounts: [],
      },
      {
        params: {
          apiKey: dbKey,
        },
        timeout: 3000,
        validateStatus: status => status < 500,
      });
  return request
    .then(result => result.data)
    .catch((error) => {
      handleError(error, 'createUser');
      return Promise.reject(error);
    });
}

/**
 * Modify User(s) from MLab".
 *
 * @param {Object} req Cloud Function request context.
 * @param {String} deleteUser delete will be performed.
 * @return {Promise} A list of users or error.
 */
async function modifyUsers(req, deleteUser) {
  console.log('> mlab.modifyUsers');
  let putData = [];

  if (!deleteUser) {
    putData = { $set: req.body };
  }
  const request = axios.put(`https://api.mlab.com/api/1/databases/techu/collections/${mlabCollection}`,
    putData,
    this.requestOptions(req));
  return request
    .then(result => result.data)
    .catch((error) => {
      handleError(error, 'modifyUsers');
      return Promise.reject(error);
    });
}

/**
 * Compose MLab query param.
 * @param {Object} id - User's id.
 * @returns {string} MLab query string.
 */
function getMLabQueryById(id) {
  let query;
  const type = utils.toType(id);

  switch (type) {
    case 'number':
      query = `{"id":${id}}`;
      break;
    default:
      query = `{"id":"${id}"}`;
  }
  return query;
}

/**
 * Compose MLab query param.
 * @param {Object} email - User's email.
 * @returns {string} MLab query string.
 */
function getMLabQueryByEmail(email) {
  return `{"email":"${email}"}`;
}

/**
 * Generate Axios Options.
 * @param {Object} req - Express request.
 * @returns {Object} Axios options.
 */
function requestOptions(req) {
  const dbKey = process.env.MLAB_API_KEY || '123';
  const options = {
    params: {},
    // Reject only if the status code is greater than or equal to 500
    validateStatus: status => status < 500,
    timeout: 3000,
  };

  if ((req.body && 'email' in req.body)
    || (req.params.user && 'email' in req.params.user)) {
    console.log('< mlab.requestOptions: query by email');
    options.params.q = this.getMLabQueryByEmail(req.body.email || req.params.user.email);
  } else if (utils.params(req)) {
    console.log('< mlab.requestOptions: query by id');
    options.params.q = this.getMLabQueryById(req.params.parts[0]);
  } else {
    console.log('< mlab.requestOptions: no query');
  }
  options.params.apiKey = dbKey;
  return options;
}

module.exports.indexUsers = indexUsers;
module.exports.createUser = createUser;
module.exports.modifyUsers = modifyUsers;
module.exports.requestOptions = requestOptions;
module.exports.getMLabQueryById = getMLabQueryById;
module.exports.getMLabQueryByEmail = getMLabQueryByEmail;
