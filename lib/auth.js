/* eslint-disable no-console */

const jwt = require('jsonwebtoken');
const jwksClient = require('jwks-rsa');
const utils = require('./utils');

const client = jwksClient({
  cache: false,
  rateLimit: true,
  jwksRequestsPerMinute: 5,
  jwksUri: 'https://techucarlosgcainzos.eu.auth0.com/.well-known/jwks.json',
});

/**
 * Verify Token data with bcrypto library.
 * @param {Object} token - Data to be hashed
 * @param {Function} cb - Callback
 * @returns {Null} - Nothing to returns
 */
function verifyToken(token, cb) {
  let decodedToken;
  try {
    decodedToken = jwt.decode(token, { complete: true });
  } catch (e) {
    console.error(e);
    cb(e);
    return;
  }
  client.getSigningKey(decodedToken.header.kid, (err, key) => {
    if (err) {
      console.error(err);
      cb(err);
      return;
    }
    const signingKey = key.publicKey || key.rsaPublicKey;
    jwt.verify(token, signingKey, (errVerify, decoded) => {
      if (errVerify) {
        console.error(errVerify);
        cb(errVerify);
        return;
      }
      cb(null, decoded);
    });
  });
}

/**
 * Verify Token issuer .
 * @param {Object} decodedToken - token claims
 * @returns {Bool} - True if authorized
 */
async function authorize(decodedToken) {
  console.log(decodedToken);
  if (decodedToken.iss === 'https://techucarlosgcainzos.eu.auth0.com/') {
    return true;
  }
  return Promise.reject(new Error('Not authorized'));
}

/**
 * Middleware to check if user is authenticated.
 * @param {Function} fn - Protected function.
 * @returns {Promise} - promise
 */
function checkAuth(fn) {
  return async (req, res) => {
    console.log('> auth.checkAuth: ');
    console.log(`{ip: "${req.ip}", "method": "${req.method}", "path": "${req.path}", params: ${req.params}}`);
    if (req.method === 'OPTIONS') {
      console.log('> auth.checkAuth OPTIONS is not AuthN || AuthZ');
      try {
        await utils.enableCORSHeaders(res);
        console.log('> auth.checkAuth OPTIONS send CORS headers');
        return res.status(200).send();
      } catch (e) {
        console.log(`CORS err: ${e}`);
        return res.status(500).send();
      }
    }
    if (!req.headers || !req.headers.authorization) {
      console.log('No authorization token found.');
      return res.status(401).send('No authorization token found.');
      // return Promise.reject(new Error('No authorization token found.'));
    }
    const parts = req.headers.authorization.split(' ');
    if (parts.length !== 2) {
      console.log('Bad credential format.');
      return res.status(401).send('Bad credential format.');
      // return Promise.reject(new Error('Bad credential format.'));
    }
    const scheme = parts[0];
    const credentials = parts[1];

    if (!/^Bearer$/i.test(scheme)) {
      console.log('Bad credential format.');
      return res.status(401).send('Bad credential format.');
      // return Promise.reject(new Error('Bad credential format.'));
    }
    return new Promise((resolve, reject) => {
      // return resolve(fn(req, res));
      verifyToken(credentials, (err, decoded) => {
        if (err) {
          console.log('Invalid token');
          res.status(401).send('Invalid token');
          return reject(new Error('Invalid token.'));
        }
        try {
          authorize(decoded);
          console.log('User is AuthN && AuthZ');
          req.params.user = decoded;
          return resolve(fn(req, res));
        } catch (errAuthZ) {
          console.log('Not authorized');
          res.status(401).send(errAuthZ.message);
          return reject(errAuthZ);
        }
      });
    });
  };
}

module.exports = checkAuth;
