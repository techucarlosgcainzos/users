/* eslint-disable no-console */

const mlab = require('../lib/mlab');
const utils = require('../lib/utils');

/**
 * create Action".
 *
 * @param {Object} req Cloud Function request context.
 * @param {Object} res Cloud Function response context.
 * @return {Array} A list of users or error.
 */
async function createAction(req, res) {
  console.log('> accountController.createAction');
  const loggeduser = req.params.user;
  let user = {};
  let modify = {};
  const performedAction = {
    action: req.body.action,
    ammount: Math.abs(Number(req.body.ammount)).toString(),
  };

  if (!utils.params(req)) {
    return res.status(400).send({ message: 'iban not found' });
  }
  const iban = req.params.parts[0];

  try {
    req.body = { email: loggeduser.email };
    user = await mlab.indexUsers(req);
    if (user.length === 0) {
      throw new Error('user not found');
    }
  } catch (error) {
    return res.status(500).send({ message: 'User not found' });
  }

  console.log(`> accountController.createAction {"user": "${loggeduser.email}","action": "${performedAction.action}", "iban": "${iban}"}`);

  if (performedAction.action === 'Deposit') {
    performedAction.validAmmount = performedAction.ammount;
  } else if (performedAction.action === 'Refund') {
    performedAction.validAmmount = performedAction.ammount * -1;
  } else {
    return res.status(400).send({ message: 'action not found' });
  }

  console.log(user);

  const accountAction = user[0].accounts.find(o => o.iban === iban);
  if (!accountAction) {
    return res.status(404).send({ message: 'iban not found' });
  }
  const accountIndex = user[0].accounts.findIndex(o => o.iban === iban);
  user[0].accounts.splice(accountIndex, 1);
  accountAction.movements.push({
    date_created: new Date(),
    ammount: performedAction.validAmmount.toString(),
  });
  accountAction.balance = await utils.sumMoney(
    accountAction.balance,
    performedAction.validAmmount.toString(),
  );
  console.log(`> accountController.createAction: action: ${accountAction}`);
  user[0].accounts.push(accountAction);

  try {
    req.body = user[0];
    modify = await mlab.modifyUsers(req, false);
  } catch (e) {
    console.log(`> accountController.createAction: error: ${e}`);
  }
  if (modify && modify.n === 1) {
    return res.status(204).send();
  }
  return res.status(404).send({ message: 'Account not modified' });
}

/**
 * create Account".
 *
 * @param {Object} req Cloud Function request context.
 * @param {Object} res Cloud Function response context.
 * @return {Array} A list of users or error.
 */
async function createAccount(req, res) {
  let user = {};
  let modify = {};
  let iban = '';
  console.log('> accountController.createAccount: ');
  const loggeduser = req.params.user;

  try {
    req.body = { email: loggeduser.email };
    user = await mlab.indexUsers(req);
  } catch (error) {
    return res.status(500).send({ message: 'User not found' });
  }

  if (user.length === 0) {
    console.log('> accountController.createUser: creating new user');
    try {
      req.body.first_name = loggeduser.name;
      req.body.last_name = '';
      req.body.email = loggeduser.email;
      user = [await mlab.createUser(req)];
      console.log(`> accountController.createUser: ${user[0].id}`);
    } catch (e) {
      return res.status(500).send({ message: 'User not found' });
    }
  }
  try {
    iban = `ES91${Math.floor(Math.random() * 99999999999999999999)}`;
    user[0].accounts.push({
      iban,
      date_created: new Date(),
      balance: '0',
      movements: [],
    });
    req.body = user[0];
    modify = await mlab.modifyUsers(req, false);
  } catch (e) {
    console.log(`> accountController.createAccount: error: ${e}`);
  }
  if (modify && modify.n === 1) {
    return res.status(201).send({ iban });
  }
  return res.status(404).send({ message: 'Account not modified' });
}

module.exports.createAction = createAction
module.exports.createAccount = createAccount;
