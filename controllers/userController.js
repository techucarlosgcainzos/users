/* eslint-disable no-console */
const mlab = require('../lib/mlab');

/**
 * Get User(s)".
 *
 * @param {Object} req Cloud Function request context.
 * @param {Object} res Cloud Function response context.
 * @return {Array} A list of users or error.
 */
async function handleUsers(req, res) {
  let user;

  console.log('> userController.handleIndex: ');
  try {
    user = await mlab.indexUsers(req);
  } catch (error) {
    return res.status(500).send({ message: error.message });
  }
  if (user.length < 1) {
    return res.status(404).send({ message: 'User not found' });
  }
  return res.status(200).send(user);
}

/**
 * Create a new User".
 *
 * @param {Object} req Cloud Function request context.
 * @param {Object} res Cloud Function response context.
 * @return {Promise} Nothing
 */
async function createUser(req, res) {
  // onst dbKey = process.env.MLAB_API_KEY || '123';

  console.log('> userController.createUser: ');
  let user;

  try {
    user = await mlab.createUser(req);
  } catch (error) {
    return res.status(500).send({ message: 'Unable to create user' });
  }
  if (user.length === 1) {
    return res.status(201).send();
  }
  return res.status(500).send({ message: 'User not created' });
}

/**
 * update a User".
 *
 * @param {Object} req Cloud Function request context.
 * @param {Object} res Cloud Function response context.
 * @return {Promise} No content.
 */
async function updateUser(req, res) {
  let modify;
  let user;

  console.log('> userController.updateUser: ');
  try {
    user = await mlab.indexUsers(req);
  } catch (error) {
    res.status(404).send({ message: 'User not found' });
  }

  if (user.length !== 1) {
    return res.status(404).send({ message: 'User not found' });
  }

  try {
    modify = await mlab.modifyUsers(req, false);
  } catch (error) {
    res.status(404).send({ message: 'User not modified' });
  }

  if (modify && modify.n === 1) {
    return res.status(204).send();
  }
  return res.status(404).send({ message: 'User not modified' });
}


/**
 * Delete an existing User".
 *
 * @param {Object} req Cloud Function request context.
 * @param {Object} res Cloud Function response context.
 * @return {Promise} Nothing
 */
async function deleteUser(req, res) {
  let del;
  console.log('> userController.deleteUser: ');
  try {
    del = await mlab.modifyUsers(req, true);
    console.log(del);
  } catch (error) {
    res.status(404).send({ message: 'User not deleted' });
  }

  if (del && del.removed === 1) {
    return res.status(204).send();
  }
  return res.status(404).send({ message: 'User not deleted' });
}

module.exports.handleUsers = handleUsers;
module.exports.createUser = createUser;
module.exports.updateUser = updateUser;
module.exports.deleteUser = deleteUser;
