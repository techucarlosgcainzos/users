[![CircleCI](https://circleci.com/bb/techucarlosgcainzos/users/tree/master.svg?style=svg)](https://circleci.com/bb/techucarlosgcainzos/users/tree/master)

[![Coverage Status](https://coveralls.io/repos/bitbucket/techucarlosgcainzos/users/badge.svg)](https://coveralls.io/bitbucket/techucarlosgcainzos/users)  
## TechU Users

### TL;DR
This repository contains backend service to manage TechU bank users.
