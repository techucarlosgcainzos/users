/* eslint-disable no-console */
const checkAuth = require('./lib/auth');
const userController = require('./controllers/userController');
const accountController = require('./controllers/accountController');
const utils = require('./lib/utils');

/**
 * accounts Cloud Function.
 *
 * @param {Object} req Cloud Function request context.
 *                     More info: https://expressjs.com/en/api.html#req
 * @param {Object} res Cloud Function response context.
 *                     More info: https://expressjs.com/en/api.html#res
 * @returns {Object} null
 */
function accounts(req, res) {
  try {
    utils.enableCORSHeaders(res);
  } catch (e) {
    console.log('> users: enableCORSHeaders failed, ', e);
  }
  switch (req.method) {
    case 'POST':
      console.log('> accounts: createAccount');
      try {
        accountController.createAccount(req, res);
      } catch (e) {
        res.status(500).send({ message: 'Bad request' });
      }
      break;
    case 'PUT':
      console.log('> accounts: createAction');
      try {
        accountController.createAction(req, res);
      } catch (e) {
        res.status(500).send({ message: 'Bad request' });
      }
      break;
    case 'OPTIONS':
      console.log('> users: OPTIONS');
      res.status(200).send();
      break;
    default:
      res.status(400).send({ message: 'Method not allowed' });
  }
}
exports.accounts = checkAuth(accounts);

/**
 * users Cloud Function.
 *
 * @param {Object} req Cloud Function request context.
 *                     More info: https://expressjs.com/en/api.html#req
 * @param {Object} res Cloud Function response context.
 *                     More info: https://expressjs.com/en/api.html#res
 * @returns {Object} null
 */
function users(req, res) {
  try {
    utils.enableCORSHeaders(res);
  } catch (e) {
    console.log('> users: enableCORSHeaders failed, ', e);
  }
  switch (req.method) {
    /** TODO: validate input user data
    *  Password must be a string
    */
    case 'GET':
      try {
        console.log('> users: handleIndex');
        userController.handleUsers(req, res);
      } catch (error) {
        console.log(error);
        res.status(400).send({ message: 'Bad request' });
      }
      break;
    case 'POST':
      try {
        console.log('> users: createUser');
        userController.createUser(req, res);
      } catch (error) {
        console.log(error);
        res.status(400).send({ message: 'Bad request' });
      }
      break;
    case 'PUT':
      try {
        console.log('> users: updateUser');
        userController.updateUser(req, res);
      } catch (error) {
        console.log(error);
        res.status(400).send({ message: 'Bad request' });
      }
      break;
    case 'DELETE':
      try {
        console.log('> users: deleteUser');
        userController.deleteUser(req, res);
      } catch (error) {
        console.log(error);
        res.status(400).send({ message: 'Bad request' });
      }
      break;
    case 'OPTIONS':
      console.log('> users: OPTIONS');
      res.status(200).send();
      break;
    default:
      res.status(400).send({ message: 'Method not allowed' });
  }
}

exports.users = checkAuth(users);
// exports.users = (users);
